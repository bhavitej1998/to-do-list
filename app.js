let express = require('express');
let bodyParser = require('body-parser');

let app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public'));

let tasks = [];
let complete = [];

const addFunction = (req, res) => {
    console.log("Request body ---" + JSON.stringify(req.body) );
    const newTask = req.body.content;
    if (newTask != "") {
        tasks.push(newTask);
    }
    res.send('The task has been added successfuly. Id for the task is ' + (tasks.length - 1));
}

const authenticator = (req, res, next) => {
    if (req.body.user === 'Bhavi') {
        console.log("I am authenticated");
        next();
    }
    else {
        res.send('Not authenticated');
    }
}

app.post('/api/task/add', authenticator, addFunction);

app.put('/api/task/complete', function (req, res) {
    const completeTaskId = req.body.taskId;
    complete.push(tasks[completeTaskId]);
    tasks.splice(completeTaskId, 1);
    res.send("The task with taskId " + completeTaskId + " has been marked complete successfully");
});

app.put('/api/task/update', function (req, res) {
    const updateTaskId = req.body.taskId;
    const updateContent = req.body.content;
    if (updateContent != "") {
        tasks[updateTaskId] = updateContent;
    }
    res.send("The task with taskId " + updateTaskId + " has been updated successfully");
});

app.delete('/api/task/remove/:taskId', function(req, res) {
    const deleteTaskId = req.params.taskId;
    tasks.splice(deleteTaskId, 1);
    res.send("The task with taskId " + deleteTaskId + " has been deleted successfully");
});

app.get("/api/task/list", function(req, res) {
    let taskList = [];
    let completeList = [];
    tasks.forEach( (task, i) => {
        taskList.push(i + ". " + task);
    } );
    complete.forEach( (c, i) => {
        completeList.push(i + ". " + c);
    })

    const list = {
        PendingTasks: taskList,
        CompletedTasks: completeList
    }
    res.send(list);
});

app.listen(8000, function () {
    console.log('Hi there');
    console.log('App listening on port 8000!')
});

