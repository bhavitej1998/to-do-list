1. Install all the node packages - npm install
2. Start the node js server - npm start
3. Go to Postman and create an environment variable on http://localhost:8000
4. Link to the Postman Collection of URLs - https://www.getpostman.com/collections/8d84d0e84e3a86001d35
5. Try out running all the requests on Postman.

